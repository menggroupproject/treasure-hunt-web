package treasure_hunt.data.dao;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.bson.types.ObjectId;
import org.xml.sax.SAXException;

import com.googlecode.mjorm.MongoDao;
import com.googlecode.mjorm.MongoDaoImpl;
import com.googlecode.mjorm.XmlDescriptorObjectMapper;
import com.mongodb.BasicDBList;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import treasure_hunt.data.models.images.ImageWrapper;
import treasure_hunt.data.models.route.Question;
import treasure_hunt.data.models.route.Route;
import treasure_hunt.data.models.route.Step;

public class RouteDao {

	private static final String ROUTES_COLLECTION = "routes";
	private static final String DB_NAME = "heroku_jbmk6wxh";
	private static final String DB_USER = "admin";
	private static final String DB_PASSWORD = "admin";
	private static final String IMAGE_COLLECTION = "images";

	private MongoDao dao;
	private DB db;

	public RouteDao(File xmlFile) throws XPathExpressionException,
			ClassNotFoundException, IOException, ParserConfigurationException,
			SAXException {
		MongoClient mongo = new MongoClient(new MongoClientURI("mongodb://"
				+ DB_USER + ":" + DB_PASSWORD
				+ "@ds047945.mongolab.com:47945/heroku_jbmk6wxh"));

		XmlDescriptorObjectMapper objectMapper = new XmlDescriptorObjectMapper();
		objectMapper.addXmlObjectDescriptor(xmlFile);

		db = mongo.getDB(DB_NAME);
		dao = new MongoDaoImpl(db, objectMapper);
	}

	public void create(Route route) {
		int i =0;
		for (Step step : route.getSteps()) {
			
			step.setStepNo(i);
			
			if (step.getTreasure() != null && !step.getTreasure().equals("")) {
				// If the string stored in step.getTreasure() is not an ID
				// but is in fact a encoded String
				if (!getIds("images").contains(step.getTreasure())) {
					ObjectId id = new ObjectId();
					ImageWrapper imageWrapper = new ImageWrapper();
					imageWrapper.set_Id(id.toHexString());
					imageWrapper.setEncodedImage(step.getTreasure());
					step.setTreasure(id.toHexString());
					dao.createObject(IMAGE_COLLECTION, imageWrapper);
				}
			}

			for (Question question : step.getQuestions()) {
				if (question.getIsImage() && question.getImage() != null
						&& !question.getImage().equals("")) {
					// If the string stored in question.getImage() is not an ID
					// but is in fact a encoded String
					if (!getIds("images").contains(question.getImage())) {
						ObjectId id2 = new ObjectId();
						ImageWrapper imageWrapper2 = new ImageWrapper();
						imageWrapper2.set_Id(id2.toHexString());
						imageWrapper2.setEncodedImage(question.getImage());
						question.setImage(id2.toHexString());
						dao.createObject(IMAGE_COLLECTION, imageWrapper2);
					}
				}
			}
			
			i++;
		}

		// Generated ID should be unique so remove should only remove old
		// versions of same object
		dao.deleteObject("routes", route.getId());
		dao.createObject(ROUTES_COLLECTION, route);
	}

	public void delete(Route route) {
		dao.deleteObject(ROUTES_COLLECTION, route.getId());
	}

	public List<Object> getIds(String collectionName) {
		List<Object> ids = new ArrayList<Object>();
		for (Object id : ((BasicDBList) db.getCollection(collectionName).distinct(
				"_id"))) {
			ids.add(id);
		}
		return ids;
	}

	public Route[] getRoutes() {
		List<Object> ids = new ArrayList<Object>();
		for (Object id : ((BasicDBList) db.getCollection("routes").distinct(
				"_id"))) {
			ids.add(id);
		}

		return dao.readObjects("routes", ids.toArray(), Route.class);
	}

	public ImageWrapper[] getImages() {
		List<Object> ids = new ArrayList<Object>();
		for (Object id : ((BasicDBList) db.getCollection("images").distinct(
				"_id"))) {
			ids.add(id);
		}

		return dao.readObjects("images", ids.toArray(), ImageWrapper.class);
	}

	public ImageWrapper getImage(String id) {
		return dao.readObject("images", id, ImageWrapper.class);
	}

}
