package treasure_hunt.data.models.images;

public class ImageWrapper {
	public String _id;
	public String encodedImage;
	
	public String get_Id() {
		return _id;
	}
	public void set_Id(String _id) {
		this._id = _id;
	}
	public String getEncodedImage() {
		return encodedImage;
	}
	public void setEncodedImage(String encodedImage) {
		this.encodedImage = encodedImage;
	}	
}
