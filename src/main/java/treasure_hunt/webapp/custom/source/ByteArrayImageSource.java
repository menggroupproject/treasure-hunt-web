package treasure_hunt.webapp.custom.source;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.vaadin.server.StreamResource.StreamSource;

public class ByteArrayImageSource implements StreamSource {
	ByteArrayOutputStream imagebuffer = null;
	int reloads = 0;
	private byte[] bytes;

	public ByteArrayImageSource(byte[] bytes) {
		this.bytes = bytes;
	}

	public InputStream getStream() {
		return new ByteArrayInputStream(bytes);
	}
}